package game.logics;

/**
 * This Enumeration is used to set the game mode.
 */
public enum GameMode {

    /**
     * This mode is set when there isn't a mode.
     */
    NO_MODE,

    /**
     * Classic game mode.
     */
    CLASSIC,

    /**
     * Alternative game mode.
     */
    ALTERNATIVE
}
